# Install R Tools 4.0
Write-Output "Installing GitLab Runners"

Write-Output "Downloading Gitlab Runners Last Version"
## download last relase name
$url = "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe"
$output = 'C:\GitLab-Runner\gitlab-runner.exe'
if ( -Not (Test-Path -Path 'C:\GitLab-Runner')) {
	New-Item -Path 'c:\' -Name "GitLab-Runner" -ItemType "directory"
}
$wcR = New-Object System.Net.WebClient

Write-Output "Downloading $url"
#Download the exe
$wcR.DownloadFile($url, $output)
Write-Output "Download completed $url"

# Add GitLab to PATH
$oldpath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
$newPath = "$oldpath;C:\GitLab-Runner\\"
Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath

Write-Output "Add gitlab server ssh key identifier to user known_hosts"
if ( -Not (Test-Path -Path $env:USERPROFILE\.ssh) ) {
	New-Item -Path $env:USERPROFILE -NAME ".ssh" -ItemType "directory"
  ssh-keyscan -t ecdsa -H gitlab.paca.inrae.fr > ~/.ssh/known_hosts
}
else {
  ssh-keyscan -t ecdsa -H gitlab.paca.inrae.fr 147.100.66.196 >> ~/.ssh/known_hosts
}

